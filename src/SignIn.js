import React, { Component } from 'react';
import unilablogo from './unilablogotrans.png';
import unilabbg from './unilabbgwhole.png';
import hatakekakashi from './hatakekakashi.png';
import styled from 'styled-components';

const BackgroundContainer = styled.div`
    width: 100%;
    height: 100%;
    position: fixed;
    left: 0px;
    top: 0px;
    z-index: -1;
`

const BackgroundImageContainer = styled.img`
    background-size: cover;
    height: 100%;
    width: auto;
`

const ModalContainer = styled.div`
    position: absolute;
    background-color: rgba(0, 0, 0, -0.5);;
    top: 0;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    min-width: 40%;
    max-width: 50%;
    height: 85%;
    z-index: 999;
    color: white;
    font-family: 'Ubuntu', sans-serif;
`

const SignInContainer = styled.div`
    position: absolute;
    background-color: #24477d;
    top: 0;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    margin: auto auto 2rem auto;
    max-width: 80%;
    height: 23rem;
    z-index: 1000;
    color: white;
    font-family: 'Ubuntu', sans-serif;
`

const HeaderContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    font-weight: 600;
    font-size: 22px;
    border-left: 0;
    color: #cacecd;
    height: 10%;
    max-width: 50%;
    margin: 1rem auto;
    color: white;
`

const HorizontalContainer = styled.div`
    border: 1px solid white;
    margin-top: 0.1rem;
    max-width: 80%;
    margin: auto;
`

const Logo = styled.img`
    width: 250px;
    height: 250px;
    max-width: 55%;
    max-height: 35%;
    margin: auto;
`

const LogoContainer = styled.div`
    height: 100%;
`

const InputFormContainer = styled.form`
    display: flex;
    align-items: center;
    flex-direction: column;
    line-height: 2;
    font-size: 12px;
    font-weight: 600;
    font-family: 'Ubuntu', sans-serif;
    margin-top: 7%;
`
const FormInputIcon = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    width: 100%;
`

const InputContainer = styled.input`
    padding: 5px;
    width: 62%;
`
const FormTextContainer = styled.span`
    width: 80%;
    text-align: left;
    font-size: 16px;
    margin: 8px 0;
`

const IconContainer = styled.i`
    width: 10%;
    margin: auto 5px;
`

const UnilabButton = styled.button`
    margin-top: 2.5rem;
    height: 2.5rem;
    width: 7.5rem;
    border-radius: 5px;
    border-style: solid;
    border-width: '2px';
    border-color: '#00B3B7';
    font-family: 'Ubuntu', sans-serif;
    background: none;
    color: white;
    font-size: 16px;
    font-weight: 600;
`

const LoggedInComponent = styled.div`
    width: 100%;
    height: 100%;
    position: fixed;
    left: 0px;
    top: 0px;
    z-index: -1;
    background-color: white;
`

const BackgroundHeader = styled.div`
    min-height: 24%;
    background-color: #24477d;
    width: 100%;
    position: relative;
    margin-bottom: 7rem;
`
const DashboardText = styled.span`
    font-size: 20px;
    font-weight: 600;
    font-family: 'Ubuntu', sans-serif;
    top: 0;
    left: 0;
    right: auto;
    position: absolute;
    padding-top: 1.5rem;
    padding-left: 2.5rem;
    color: white;
`

const LogoutButton = styled.button`
    margin-top: 1rem;
    height: 5%;
    width: 5%;
    max-height: 7%;
    max-width: 7%;
    border-radius: 5px;
    border-style: solid;
    border-width: '2px';
    border-color: '#00B3B7';
    font-family: 'Ubuntu', sans-serif;
    background: none;
    color: white;
    font-size: 30px;
    font-weight: 600;
    top: 0;
    left: 92%;
    right: 0;
    bottom: 0;
    position: absolute;
    z-index: 3;
`

const ProfileTextContainer = styled.div`
    color: white;
    display: flex;
    align-items: left;
    flex-direction: column;
    z-index: 7;
    top: 16%;
    left: 21%;
    position: absolute;
`

const NameContainer = styled.div`
    font-size: 24px;
    font-weight: 600;
`

const AddressContainer = styled.div`
    font-size: 18px;
    font-weight: 400;
`

const NavigateAppContainer = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    height: 55%;
    justify-content: space-evenly;
    padding: auto;
    z-index: 20;
    margin: 0 2.5rem;
`

const NavAppButtonContainer = styled.button`
    width: 28%;
    margin-top: 2rem;
    background: none;
    border: 2px #24477d solid
    border-radius: 10%;
`

const CircularImage = styled.img`
    border-radius: 50%;
    height: 150px;
    min-width: 150px;
    border: 5px solid white;
    background-color: white;
    position: absolute;
    top: 13%;
    left: 3%;
    right: 0;
    bottom: 0;
    z-index: 6;
`

export class SignIn extends Component {
    constructor(props){
        super(props);
        this.state = {
            active: "Logout"
        }
    }

    showPage(active_page) {
        this.setState({ active: active_page })
    }


    render() {

        const { active } = this.state;

        if (active === "Logout") {
            return (
                <LoggedInComponent>
                    <BackgroundHeader />
                    <LogoutButton onClick={ this.showPage.bind(this, "Login") }>
                        X
                    </LogoutButton>
                    <ProfileTextContainer>
                        <NameContainer>Hatake Kakashi</NameContainer>
                        <AddressContainer>Konoha Village</AddressContainer>
                    </ProfileTextContainer>
                    <DashboardText>DASHBOARD</DashboardText>
                    <NavigateAppContainer>
                        <NavAppButtonContainer onClick={ this.showPage.bind(this, "Login") }/>
                        <NavAppButtonContainer onClick={ this.showPage.bind(this, "Login") }/>
                        <NavAppButtonContainer onClick={ this.showPage.bind(this, "Login") }/>
                        <NavAppButtonContainer onClick={ this.showPage.bind(this, "Login") }/>
                        <NavAppButtonContainer onClick={ this.showPage.bind(this, "Login") }/>
                        <NavAppButtonContainer onClick={ this.showPage.bind(this, "Login") }/>
                    </NavigateAppContainer>
                    <CircularImage src={hatakekakashi}/>
                </LoggedInComponent>
            );
        } else {
            return (
                <div>
                    <BackgroundContainer>
                        <BackgroundImageContainer src={unilabbg} />
                    </BackgroundContainer>
                    <ModalContainer>
                        <LogoContainer >
                            <Logo src={unilablogo} />
                        </LogoContainer>
                        <SignInContainer>
                            <HeaderContainer>
                                Sign In
                            </HeaderContainer>
                            <HorizontalContainer />
                            <InputFormContainer >
                                <FormTextContainer>Username</FormTextContainer>
                                <FormInputIcon>
                                    <IconContainer className="fa fa-user icon" />
                                    <InputContainer name="username" type="username" required />
                                </FormInputIcon>
                                <FormTextContainer>Password</FormTextContainer>
                                <FormInputIcon>
                                    <IconContainer className="fa fa-key icon" />
                                    <InputContainer name="password" type="password" required />
                                </FormInputIcon>
                            </InputFormContainer>
                            <UnilabButton onClick={ this.showPage.bind(this, "Logout") }>
                                Login
                            </UnilabButton>
                        </SignInContainer>
                    </ModalContainer>
                </div>
            );
        }
    }
}

export default SignIn;
